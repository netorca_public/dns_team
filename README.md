# DNS Team service examples

This is a demonastration of a how team managing the DNS/DDI infrastructure could define and create simple dns services. 


## Services

The services defined here are:
* a_record: A demonstration of how to define a a_record, both ipv4_address and url are validated by regex
* cname: A demonstration of how to define a cname record, both url and canonical are validated by regex.